---
title: "Enriched Heatmap"
output:
  html_document:
    df_print: paged
editor_options:
  chunk_output_type: console
---

# Library
```{r}
library(EnrichedHeatmap)
library(circlize)
```

# Data
```{r}
set.seed(123)
load(system.file("extdata", "chr21_test_data.RData", package = "EnrichedHeatmap"))
ls()
```

```{r}
cgi
genes
H3K4me3
meth
```


```{r}
tss <- promoters(genes, upstream = 0, downstream = 1)
tss
```

```{r}
mat1 <- normalizeToMatrix(H3K4me3, tss,
  value_column = "coverage",
  extend = 5000, mean_mode = "w0", w = 50
)
mat1
```


```{r}
EnrichedHeatmap(mat1, name = "H3K4me3")
```

```{r}
col_fun <- colorRamp2(quantile(mat1, c(0, 0.99)), c("white", "red"))
EnrichedHeatmap(mat1,
  col = col_fun, name = "H3K4me3",
  row_split = sample(c("A", "B"), length(genes), replace = TRUE),
  column_title = "Enrichment of H3K4me3"
)
```

```{r}
set.seed(123)
EnrichedHeatmap(mat1,
  col = col_fun, name = "H3K4me3", row_km = 3,
  column_title = "Enrichment of H3K4me3", row_title_rot = 0
)
```

```{r}
mat2 <- normalizeToMatrix(meth, tss,
  value_column = "meth", mean_mode = "absolute",
  extend = 5000, w = 50, background = NA
)
meth_col_fun <- colorRamp2(c(0, 0.5, 1), c("blue", "white", "red"))
EnrichedHeatmap(mat2, col = meth_col_fun, name = "methylation", column_title = "methylation near TSS")
```


```{r}
mat2 <- normalizeToMatrix(meth, tss,
  value_column = "meth", mean_mode = "absolute",
  extend = 5000, w = 50, background = NA, smooth = TRUE
)
EnrichedHeatmap(mat2, col = meth_col_fun, name = "methylation", column_title = "methylation near TSS")
```


```{r}
EnrichedHeatmap(mat1,
  col = col_fun, name = "H3K4me3",
  top_annotation = HeatmapAnnotation(enrich = anno_enriched(axis_param = list(side = "left")))
) +
  EnrichedHeatmap(mat2, col = meth_col_fun, name = "methylation") +
  Heatmap(log2(rpkm + 1),
    col = c("white", "orange"), name = "log2(rpkm+1)",
    show_row_names = FALSE, width = unit(5, "mm")
  )
```


```{r}
partition <- paste0("cluster", kmeans(mat1, centers = 3)$cluster)
lgd <- Legend(
  at = c("cluster1", "cluster2", "cluster3"), title = "Clusters",
  type = "lines", legend_gp = gpar(col = 2:4)
)
ht_list <- Heatmap(partition,
  col = structure(2:4, names = paste0("cluster", 1:3)), name = "partition",
  show_row_names = FALSE, width = unit(3, "mm")
) +
  EnrichedHeatmap(mat1,
    col = col_fun, name = "H3K4me3",
    top_annotation = HeatmapAnnotation(lines = anno_enriched(gp = gpar(col = 2:4))),
    column_title = "H3K4me3"
  ) +
  EnrichedHeatmap(mat2,
    col = meth_col_fun, name = "methylation",
    top_annotation = HeatmapAnnotation(lines = anno_enriched(gp = gpar(col = 2:4))),
    column_title = "Methylation"
  ) +
  Heatmap(log2(rpkm + 1),
    col = c("white", "orange"), name = "log2(rpkm+1)",
    show_row_names = FALSE, width = unit(15, "mm"),
    top_annotation = HeatmapAnnotation(summary = anno_summary(
      gp = gpar(fill = 2:4),
      outline = FALSE, axis_param = list(side = "right")
    ))
  )
draw(ht_list,
  split = partition, annotation_legend_list = list(lgd),
  ht_gap = unit(c(2, 8, 8), "mm")
)
```
