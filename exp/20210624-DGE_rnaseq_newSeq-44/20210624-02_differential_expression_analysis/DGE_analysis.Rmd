---
title: "RNA-Seq: differential expression analysis of controls (lab + literature)"
author: "Deepak Tanwar"
date: "<b>Created on:</b> 2021-06-24 <br> <b>Updated on:</b> `r Sys.Date()`"
output:
  rmdformats::readthedown:
    highlight: tango
    self_contained: true
    thumbnails: true
    lightbox: true
    gallery: false
    keep_md: no
    number_sections: no
    fig_width: 8
    fig_height: 8
    fig_caption: true
    df_print: paged
    code_folding: show
  fontsize: 12pt
  geometry: margin=1in
  documentclass: article
# bibliography: references.bib
link-citations: yes
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Libraries required

```{r, message=FALSE, warning=FALSE}
library(limma)
library(edgeR)
library(plgINS)
library(sva)
library(ggplot2)
library(dplyr)
library(patchwork)
library(SummarizedExperiment)
library(reshape2)
library(autoplotly)
library(pheatmap)
library(viridis)
library(RColorBrewer)
library(ggplotify)
library(SEtools)
library(DESeq2)
```

# Load salmon object
```{r, message=FALSE, warning=FALSE}
load("input/SC_controls_rnaseq_lab_june2021.tds.RData")
```

# Differeitial analysis using `limma`

## PND8 vs PND15
```{r }
design <- model.matrix(~ 0 + Group + LibPrepBatch,
  data = salmon@phenoData[grep(pattern = "PND8|PND15", x = salmon@phenoData$Group), ]
)
colnames(design) <- gsub(pattern = "Group", replacement = "", x = colnames(design))

y <- DGEList(counts = salmon@gene.counts[, grep(pattern = "PND8|PND15", x = colnames(salmon@gene.counts))])
keep <- filterByExpr(y, design, min.count = 15)
y <- y[keep, ]

mod <- model.matrix(~ LibPrepBatch + Group,
  data = salmon@phenoData[grep(pattern = "PND8|PND15", x = salmon@phenoData$Group), ]
)

mod0 <- model.matrix(~LibPrepBatch,
  data = salmon@phenoData[grep(pattern = "PND8|PND15", x = salmon@phenoData$Group), ]
)

sv <- svaseq(dat = y$counts, mod = mod, mod0 = mod0, numSVmethod = "leek")$sv


colnames(sv) <- paste0("SV", 1:ncol(sv))
design <- cbind(sv, design)

dds <- calcNormFactors(y)

v <- voom(dds, design = design)

contrast.matrix <- makeContrasts(PND15 - PND8, levels = design)
fit <- lmFit(v)

fit2 <- contrasts.fit(fit, contrast.matrix)
fit2 <- eBayes(fit2)

pnd8.pnd15 <- as.data.frame(topTable(fit2, coef = 1, number = Inf))
pnd8.pnd15 <- data.frame(Genes = rownames(pnd8.pnd15), pnd8.pnd15, stringsAsFactors = F)
pnd8.pnd15_sig <- pnd8.pnd15[abs(pnd8.pnd15$logFC) >= 1 & pnd8.pnd15$adj.P.Val <= 0.05, ]
```


## PND15 vs PNW21
```{r }
design <- model.matrix(~ 0 + Group + LibPrepBatch,
  data = salmon@phenoData[grep(pattern = "PND15|PNW21", x = salmon@phenoData$Group), ]
)
colnames(design) <- gsub(pattern = "Group", replacement = "", x = colnames(design))

y <- DGEList(counts = salmon@gene.counts[, grep(pattern = "PND15|PNW21", x = colnames(salmon@gene.counts))])
keep <- filterByExpr(y, design, min.count = 15)
y <- y[keep, ]

mod <- model.matrix(~ LibPrepBatch + Group,
  data = salmon@phenoData[grep(pattern = "PND15|PNW21", x = salmon@phenoData$Group), ]
)

mod0 <- model.matrix(~LibPrepBatch,
  data = salmon@phenoData[grep(pattern = "PND15|PNW21", x = salmon@phenoData$Group), ]
)

sv <- svaseq(dat = y$counts, mod = mod, mod0 = mod0, numSVmethod = "leek")$sv

colnames(sv) <- paste0("SV", 1:ncol(sv))
design <- cbind(sv, design)

dds <- calcNormFactors(y)
v <- voom(dds, design = design)

contrast.matrix <- makeContrasts(PNW21 - PND15, levels = design)

fit <- lmFit(v)

fit2 <- contrasts.fit(fit, contrast.matrix)
fit2 <- eBayes(fit2)

pnd15.pnw21 <- as.data.frame(topTable(fit2, coef = 1, number = Inf))
pnd15.pnw21 <- data.frame(Genes = rownames(pnd15.pnw21), pnd15.pnw21, stringsAsFactors = F)
pnd15.pnw21_sig <- pnd15.pnw21[abs(pnd15.pnw21$logFC) >= 1 & pnd15.pnw21$adj.P.Val <= 0.05, ]
```


# `cpm` Counts data and pData

```{r }
y <- DGEList(counts = salmon@gene.counts)
keep <- filterByExpr(y, design, min.count = 15)
y <- y[keep, ]

design <- model.matrix(~ 0 + Group + LibPrepBatch, data = salmon@phenoData)

mod <- model.matrix(~ LibPrepBatch + Group, data = salmon@phenoData)

mod0 <- model.matrix(~LibPrepBatch, data = salmon@phenoData)

sv <- svacor(y$counts, mm = mod, mm0 = mod0, numSVmethod = "leek")

dds <- calcNormFactors(y, design = design)
cpm <- cpm(dds, log = T)

data <- list(cpm = cpm, sva = sv$cor, pData = salmon@phenoData)

save(data,
  file = "./output/data_pData.RData", compress = T,
  compression_level = 3
)
```

# Results
```{r }
dea.list <- list(
  `pnd15 vs pnd8` = as.DEA(pnd8.pnd15),
  `pnw21 vs pnd15` = as.DEA(pnd15.pnw21)
)

dea.limma <- list(
  `pnd15 vs pnd8` = pnd8.pnd15,
  `pnw21 vs pnd15` = pnd15.pnw21
)
```


## Save RData files
```{r }
save(dea.list,
  file = "./output/dea_SC_Controls_lab_newSeq.DEA.RData", compress = T,
  compression_level = 3
)

save(dea.limma,
  file = "./output/limma_SC_Controls_lab_newSeq.RData", compress = T,
  compression_level = 3
)

writexl::write_xlsx(x = dea.limma, path = "output/dea_results.xlsx", col_names = T, format_headers = T)
```


# MA plots
## PND15 vs PND8
```{r, warning=FALSE, message=FALSE}
res_8_15 <- pnd8.pnd15
res_8_15$threshold <- as.factor(res_8_15$adj.P.Val < 0.05)

p1 <- ggplot(data = res_8_15, aes(
  x = res_8_15$AveExpr,
  y = res_8_15$logFC,
  colour = threshold
)) +
  geom_point(alpha = 0.5, size = 1.8) +
  geom_hline(aes(yintercept = 0), colour = "blue", size = 1) +
  ylim(c(
    -ceiling(max(abs(res_8_15$logFC))),
    ceiling(max(abs(res_8_15$logFC)))
  )) +
  ggtitle("PND15 vs PND8") +
  labs(subtitle = "loess fit") +
  xlab("Mean expression") +
  ylab("Log2 Fold Change") +
  theme(
    plot.title = element_text(face = "bold", size = 20, hjust = 0.5),
    plot.subtitle = element_text(size = 16, hjust = 0.5, face = "italic", color = "blue"),
    axis.title.x = element_text(face = "bold", size = 15),
    axis.text.x = element_text(face = "bold", size = 12),
    legend.title = element_text(face = "bold", size = 15),
    legend.text = element_text(size = 14)
  ) +
  scale_colour_discrete(name = "p.adjusted < 0.05") +
  stat_smooth(se = FALSE, method = "loess", color = "red", formula = y ~ x, size = 1)
```

```{r, fig.height=5, fig.width=11}
p1
```

## PNW21 vs PND15
```{r, warning=FALSE, message=FALSE}
res_15_21 <- pnd15.pnw21
res_15_21$threshold <- as.factor(res_15_21$adj.P.Val < 0.05)

p2 <- ggplot(data = res_15_21, aes(
  x = res_15_21$AveExpr,
  y = res_15_21$logFC,
  colour = threshold
)) +
  geom_point(alpha = 0.5, size = 1.8) +
  geom_hline(aes(yintercept = 0), colour = "blue", size = 1) +
  ylim(c(
    -ceiling(max(abs(res_15_21$logFC))),
    ceiling(max(abs(res_15_21$logFC)))
  )) +
  ggtitle("PNW21 vs PND15") +
  labs(subtitle = "loess fit") +
  xlab("Mean expression") +
  ylab("Log2 Fold Change") +
  theme(
    plot.title = element_text(face = "bold", size = 20, hjust = 0.5),
    plot.subtitle = element_text(size = 16, hjust = 0.5, face = "italic", color = "blue"),
    axis.title.x = element_text(face = "bold", size = 15),
    axis.text.x = element_text(face = "bold", size = 12),
    legend.title = element_text(face = "bold", size = 15),
    legend.text = element_text(size = 14)
  ) +
  scale_colour_discrete(name = "p.adjusted < 0.05") +
  stat_smooth(se = FALSE, method = "loess", color = "red", formula = y ~ x, size = 1)
```

```{r, fig.height=5, fig.width=11}
p2
```



# Heatmap of most variable genes
```{r}
xvar <- apply((data$cpm + 1), 1, var)
genes500 <- head(sort(xvar, decreasing = TRUE), n = 500)
x500 <- data$cpm[rownames(data$cpm) %in% names(genes500), ]

pheatmap(x500,
  scale = "row", show_rownames = F, col = viridis(100),
  show_colnames = F, annotation_col = data$pData[, 1:2],
  main = "Clustering of samples based on top 500 variable genes"
)
```


# PCA {.tabset .tabset-pills}

## CPM
```{r, fig.height=8.5, fig.width=11, warning=FALSE, message=FALSE}
plgINS::plPCA(
  x = data$cpm, samples_data = data$pData,
  colorBy = "Group", shapeBy = "LibPrepBatch", add.labels = FALSE
)
```

## SVA normalized
```{r, fig.height=8.5, fig.width=11, warning=FALSE, message=FALSE}
plgINS::plPCA(
  x = data$sva, samples_data = data$pData,
  colorBy = "Group", shapeBy = "LibPrepBatch", add.labels = FALSE
)
```






# Volcano plots
## PND15 vs PND8
```{r, warning=FALSE, message=FALSE}
res_8_15 <- pnd8.pnd15
res_8_15$category <- "PND15 vs PND8"

tab <- rbind(res_8_15)

tab_lfc <- tab[abs(tab$logFC) >= 1, ]
tab_fdr <- tab[tab$adj.P.Val <= 0.05, ]
tab_lfc_fdr <- tab[abs(tab$logFC) >= 1 & tab$adj.P.Val <= 0.05, ]
tab$Significant <- "Not significant"
tab$Significant[tab$adj.P.Val <= 0.05 & abs(tab$logFC) >= 1] <- "Down regulated"
# tab$Significant[tab$`diffAccessibility-qvalue` <= 0.05 & tab$`diffAccessibility-logFC` >= 1] <- "Positive"
tab$Significant[tab$Significant == "Down regulated"] <- ifelse(tab$logFC[tab$Significant == "Down regulated"] > 0, "Up regulated", "Down regulated")
tab$Significant <- factor(x = tab$Significant, levels = unique(tab$Significant))
colnames(tab)[6] <- "qvalue"

anno <- data.frame(
  x = c(-7, 7),
  y = c(6, 6),
  label = c("n = 146", "n = 517"), 
  col = c("#0072B5FF", "#BC3C29FF")
)


p1 <- ggplot(tab, aes(x = logFC, y = -log10(qvalue))) +
  geom_point(aes(color = Significant), size = 2, alpha = 0.5) +
  scale_color_manual(values = c("#BC3C29FF", "#0072B5FF", "black")) +
  xlim(
    -(ceiling(max(abs(tab$logFC)))),
    ceiling(max(abs(tab$logFC)))
  ) +
  ylim(0, max(-log10(tab$qvalue), na.rm = TRUE) + 1) +
  xlab(bquote(~ Log[2] ~ "FC")) +
  ylab(bquote(~ -Log[10] ~ adjusted ~ italic(P))) +
  labs(
    title = "",
    subtitle = "", 
    caption = paste0("Total = ", scales::comma(nrow(tab)), " tested genes")
  ) +
  # theme_bw(base_family = "Helvetica") +
  theme_bw()+
  theme(
    legend.title = element_blank(),
    plot.caption = element_text(size = 12),
    # legend.justification = "bottom",
    legend.position = "none",
    axis.title=element_text(size=12),
    axis.text = element_text(size=10, color = "black"),
    legend.text = element_text(size=10),
    strip.text.x = element_text(size = 12, color = "black"),
    strip.background =element_rect(fill="white")
  ) + geom_vline(
    xintercept = c(-1, 1),
    linetype = "longdash",
    colour = "black",
    size = 0.4
  ) +
  geom_hline(
    yintercept = -log10(0.05),
    linetype = "longdash",
    colour = "black",
    size = 0.4
  ) +
    facet_wrap(~category, scales = "free") +
  guides(
    colour = guide_legend(
      order = 2,
      override.aes = list(
        size = 5
      )
    )
  ) +
  scale_shape_manual(
    values = c(
      `Not significant` = 19,
      Positive = 19,
      Negative = 19
    ),
    labels = c(
      `Not significant` = "Not significant",
      Positive = "Up regulated",
      Negative = "Down regulated"
    )
  ) +
  guides(color = guide_legend(override.aes = list(size = 3, alpha = 1))) +
  geom_label(
    data = anno, aes(x = x, y = y, label = label),
    color = c("#0072B5FF", "#BC3C29FF"),
    size = 3, angle = 45, fontface = "plain", family = "Helvetica"
  )

p1
# anno <- data.frame(
#   x = c(-10, 10),
#   y = c(7, 7),
#   label = c("n = 146", "n = 517", "n = 914", "n = 1579"), 
#   col = c("#0072B5FF", "#BC3C29FF", "#0072B5FF", "#BC3C29FF")
# )
```

## PNW21 vs PND15
```{r, warning=FALSE, message=FALSE}
res_15_21 <- pnd15.pnw21
res_15_21$category <- "PNW21 vs PND15"
# tab <- rbind(res_8_15, res_15_21)

tab <- rbind(res_15_21)

tab_lfc <- tab[abs(tab$logFC) >= 1, ]
tab_fdr <- tab[tab$adj.P.Val <= 0.05, ]
tab_lfc_fdr <- tab[abs(tab$logFC) >= 1 & tab$adj.P.Val <= 0.05, ]
tab$Significant <- "Not significant"
tab$Significant[tab$adj.P.Val <= 0.05 & abs(tab$logFC) >= 1] <- "Down regulated"
# tab$Significant[tab$`diffAccessibility-qvalue` <= 0.05 & tab$`diffAccessibility-logFC` >= 1] <- "Positive"
tab$Significant[tab$Significant == "Down regulated"] <- ifelse(tab$logFC[tab$Significant == "Down regulated"] > 0, "Up regulated", "Down regulated")
tab$Significant <- factor(x = tab$Significant, levels = unique(tab$Significant)[c(2,1,3)])
colnames(tab)[6] <- "qvalue"

anno <- data.frame(
  x = c(-14, 14),
  y = c(7, 7),
  label = c("n = 914", "n = 1579"),
  col = c("#0072B5FF", "#BC3C29FF")
)

p2 <- ggplot(tab, aes(x = logFC, y = -log10(qvalue))) +
  geom_point(aes(color = Significant), size = 2, alpha = 0.5) +
  scale_color_manual(values = c("#BC3C29FF", "#0072B5FF", "black")) +
  xlim(
    -(ceiling(max(abs(tab$logFC)))),
    ceiling(max(abs(tab$logFC)))
  ) +
  ylim(0, max(-log10(tab$qvalue), na.rm = TRUE) + 1) +
  xlab(bquote(~ Log[2] ~ "FC")) +
  ylab("") +
  labs(
    title = "",
    subtitle = "", caption = paste0("Total = ", scales::comma(nrow(tab)), " tested genes")
  ) +
  # theme_bw(base_family = "Helvetica") +
  theme_bw()+
  theme(
    legend.position = "right",
    legend.title = element_blank(),
    plot.caption = element_text(size = 12),
    axis.title=element_text(size=12),
    axis.text = element_text(size=10, color = "black"),
    legend.text = element_text(size=10),
    strip.text.x = element_text(size = 12, color = "black"),
    strip.background =element_rect(fill="white")
  ) + geom_vline(
    xintercept = c(-1, 1),
    linetype = "longdash",
    colour = "black",
    size = 0.4
  ) +
  geom_hline(
    yintercept = -log10(0.05),
    linetype = "longdash",
    colour = "black",
    size = 0.4
  ) +
    facet_wrap(~category, scales = "free") +
  guides(
    colour = guide_legend(
      order = 2,
      override.aes = list(
        size = 5
      )
    )
  ) +
  scale_shape_manual(
    values = c(
      `Not significant` = 19,
      Positive = 19,
      Negative = 19
    ),
    labels = c(
      `Not significant` = "Not significant",
      Positive = "Up regulated",
      Negative = "Down regulated"
    )
  ) +
  guides(color = guide_legend(override.aes = list(size = 3, alpha = 1))) +
  geom_label(
    data = anno, aes(x = x, y = y, label = label),
    color = c("#0072B5FF", "#BC3C29FF"),
    size = 3, angle = 45, fontface = "plain", family = "Helvetica"
  )

p2
```

```{r}
p1|p2
```




# References
```{r }
report::cite_packages(session = sessionInfo())
```

# SessionInfo
```{r }
devtools::session_info() %>%
  details::details()
```
