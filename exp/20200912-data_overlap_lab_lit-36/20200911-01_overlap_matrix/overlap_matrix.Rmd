---
title: "Overlap matrix"
author: "Deepak Tanwar"
date: "<b>Created on:</b> 2020-09-11 <br> <b>Updated on:</b> `r Sys.Date()`"
output:
  rmdformats::readthedown:
    highlight: tango
    self_contained: true
    thumbnails: true
    lightbox: true
    gallery: false
    keep_md: no
    number_sections: no
    fig_width: 8
    fig_height: 8
    fig_caption: true
    df_print: paged
    code_folding: show
  fontsize: 12pt
  geometry: margin=1in
  documentclass: article
# bibliography: references.bib
link-citations: yes
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE, message = FALSE, warning = FALSE)
```


# Library
```{r overlap-matrix-1, warning = F, message = F}
library(data.table)
library(tidyverse)
library(plgINS)
library(csaw)
library(gUtils)
library(edgeR)
library(report)
```

# ATAC-Seq
```{r overlap-matrix-2, message=FALSE, warning=FALSE}
# SE object
load("input/atacseq/atac_diff_chromatin_accessibility.RData")

# CPM of ATAC-Seq data
atac <- data
cpm_atac <- cpm(asDGEList(atac))
colnames(cpm_atac) <- gsub(pattern = ".*/|\\.bam", replacement = "", x = colData(atac)[, 1])
rownames(cpm_atac) <- rowData(atac)[, 6]

# ATAC peaks
rd_atac <- rowData(atac)[, 1:6]
colnames(rd_atac) <- gsub(pattern = "Peak-", replacement = "", x = colnames(rd_atac))
rd_atac <- GRanges(rd_atac)

# Re-annotation of peaks
peakAnno <- plgINS:::annoPeaks(
  peaks = rd_atac, 
  gtf ="/mnt/IM/reference/annotation/gencodeM18/gencode.vM18.chr_patch_hapl_scaff.annotation.gtf.gz", 
  proximal = 2500
)
anno_rd <- data.frame(peakAnno, stringsAsFactors = FALSE)
colnames(anno_rd)[6] <- paste("Peak", colnames(anno_rd)[6], sep = "-")

# Results of diff analysis
r <- data.frame(rowData(data), check.names = F, stringsAsFactors = FALSE)

# Merging results with new annotation
res <- inner_join(x = r[, -c(12:16)], y = anno_rd[, -c(1:5)])
res <- res[, c(
  grep(pattern = "diffAccessibility", x = colnames(res), invert = TRUE),
  grep(pattern = "diffAccessibility", x = colnames(res))
)]

colnames(res) <- gsub(pattern = "Peak-", replacement = "", x = colnames(res))

# Subset significant results
res_lp <- res[res$`diffAccessibility-qvalue` <= 0.05 & abs(res$`diffAccessibility-logFC`) > 1, -c(7:11)]
rownames(res_lp) <- res_lp$Name

# Adding Enrichment value to table
res_lp$ATAC_Adult <- rowSums(cpm_atac[res_lp$Name, grep(pattern = "Adult", x = colnames(cpm_atac))])
res_lp$ATAC_PND15 <- rowSums(cpm_atac[res_lp$Name, grep(pattern = "PND15", x = colnames(cpm_atac))])

# Coping data to variable
tab <- res_lp

# GRanges to overlap with
gr <- GRanges(res_lp[, 1:6])
```


# ChIP-Seq
```{r overlap-matrix-3 }
# Reading ChIP data
chip_files <- list.files("input/chipseq", full.names = TRUE)
names(chip_files) <- gsub(pattern = "_SRX.*", replacement = "", x = basename(chip_files))
chip_peaks <- lapply(chip_files, function(x) {
  read.table(x,
    header = F,
    col.names = c(
      "Chr", "Start", "End", "Name", "Score", "Strand",
      "FoldChange", "negLog10pvalue", "negLog10qvalue",
      "Summit"
    ),
    stringsAsFactors = F
  )
})


# Overlap ChIP peaks with ATAC differential accessible peaks
overlap_chip <- lapply(chip_peaks, function(x) names(metaGR(gr1 = gr, gr2 = GRanges(x), minOverlap = 10)))

# Adding logical values based on overlap to the main table
tab$ChIP_PNW8_H3K4me3 <- tab$ChIP_PNW8_H3K27me3 <- tab$ChIP_PNW8_H3K27ac <- FALSE
tab[overlap_chip$PNW8_H3K4me3, "ChIP_PNW8_H3K4me3"] <- TRUE
tab[overlap_chip$PNW8_H3K27me3, "ChIP_PNW8_H3K27me3"] <- TRUE
tab[overlap_chip$PNW8_H3K27ac, "ChIP_PNW8_H3K27ac"] <- TRUE
```


# Bisulphite-Seq
```{r overlap-matrix-4 }
# Reading BS coverage files generated by bismark
nm_bs <- "./output/bs_cov.RData"

if (!file.exists(nm_bs)) {
  bs_files <- list.files("input/bsseq", pattern = "\\.gz$", full.names = TRUE)
  names(bs_files) <- gsub(
    pattern = "_SRX.*|_1_val_1_bismark_bt2_pe.bismark.cov.gz",
    replacement = "", x = basename(bs_files)
  )

  bs_cov <- lapply(bs_files, function(x) {
    fread(
      input = x,
      sep = "\t", quote = F, stringsAsFactors = F,
      data.table = FALSE, nThread = detectCores(), showProgress = F,
      col.names = c("seqnames", "start", "end", "percent", "Me", "Un")
    )
  })

  save(bs_cov, file = nm_bs)
} else {
  load(nm_bs)
}

# Overlap BSseq files with differential accessible peaks
overlap_bs <- lapply(bs_cov, function(x) {
  a <- metaGR(gr1 = gr, gr2 = GRanges(x), minOverlap = 1) # overlap
  b <- data.frame(a, check.names = F, stringsAsFactors = F) # converting to data frame
  c <- b[, grep(pattern = "GRanges", x = colnames(b), invert = TRUE)] # Removing GRanges of methylation data
  d <- split(x = c, f = c$Name) # Split on the basis of peak names
  e <- lapply(d, function(y) { # For each peak region
    f <- y[1, ]
    f$percent <- paste(y$percent / 100, collapse = ",") # percent to 0-1 range
    f$Me <- paste(y$Me, collapse = ",") # keeping all values
    f$Un <- paste(y$Un, collapse = ",") # keeping all values
    f$bp <- nrow(y) # Number of CpGs
    f$Me_mean <- mean(y$Me) # Mean
    f$Un_mean <- mean(y$Un) # Mean
    f$meth <- f$Me_mean / (f$Me_mean + f$Un_mean) # methylation value calculation
    return(f)
  })
  g <- plyr::ldply(e, data.frame)[, -1] # list of DF to DF
  return(g)
})

# Changing column names of data
for (i in 1:length(overlap_bs)) {
  n <- names(overlap_bs)[i]
  colnames(overlap_bs[[i]])[7:ncol(overlap_bs[[i]])] <- paste("BS", n,
    colnames(overlap_bs[[i]])[7:ncol(overlap_bs[[i]])],
    sep = "_"
  )
}

# Adding BS data to the main table
tab <- left_join(x = tab, y = overlap_bs$PND7)
tab <- left_join(x = tab, y = overlap_bs$PND14)
tab <- left_join(x = tab, y = overlap_bs$PNW8)
```


## RRBS
```{r overlap-matrix-5 }
# Read RRBs file
rrbs_file <- "input/rrbs/PND8_trimmed_bismark_bt2.bismark.cov.gz"
names(rrbs_file) <- gsub(
  pattern = "_trimmed_bismark_bt2.bismark.cov.gz",
  replacement = "", x = basename(rrbs_file)
)

rrbs_file <- as.list(rrbs_file)

rrbs_cov <- lapply(rrbs_file, function(x) {
  fread(
    input = x,
    sep = "\t", quote = F, stringsAsFactors = F,
    data.table = FALSE, nThread = detectCores(), showProgress = F,
    col.names = c("seqnames", "start", "end", "percent", "Me", "Un")
  )
})

# Overlap similar to BS data
overlap_rrbs <- lapply(rrbs_cov, function(x) {
  a <- metaGR(gr1 = gr, gr2 = GRanges(x), minOverlap = 1)
  b <- data.frame(a, check.names = F, stringsAsFactors = F)
  c <- b[, grep(pattern = "GRanges", x = colnames(b), invert = TRUE)]
  d <- split(x = c, f = c$Name)
  e <- lapply(d, function(y) {
    f <- y[1, ]
    f$percent <- paste(y$percent / 100, collapse = ",")
    f$Me <- paste(y$Me, collapse = ",")
    f$Un <- paste(y$Un, collapse = ",")
    f$bp <- nrow(y)
    f$Me_mean <- mean(y$Me)
    f$Un_mean <- mean(y$Un)
    f$meth <- f$Me_mean / (f$Me_mean + f$Un_mean)
    return(f)
  })
  g <- plyr::ldply(e, data.frame)[, -1]
  return(g)
})

# Changeing column names
for (i in 1:length(overlap_rrbs)) {
  n <- names(overlap_rrbs)[i]
  colnames(overlap_rrbs[[i]])[7:ncol(overlap_rrbs[[i]])] <- paste("RRBS", n, colnames(overlap_rrbs[[i]])[7:ncol(overlap_rrbs[[i]])], sep = "_")
}

# Adding RRBS data to the main table
tab <- left_join(x = tab, y = overlap_rrbs$PND8)
```

# RNA-Seq
```{r overlap-matrix-6 }
# CPM
load("input/rnaseq/data_pData.RData")

# limma analysis results
load("input/rnaseq/limma_SC_Controls_lab_lit.RData")

# Mean of CPM values
cpm <- data$cpm
mat_RNA_counts_merged <- data.frame(
  Genes = rownames(cpm),
  PNW8 = cpm[, grep(pattern = "PNW8", x = colnames(cpm))],
  PND14 = cpm[, grep(pattern = "PND14", x = colnames(cpm))],
  PND8 = rowMeans(cpm[, grep(pattern = "PND8", x = colnames(cpm))])
)

# Changing column names
names(dea.limma) <- toupper(names(dea.limma))
names(dea.limma) <- gsub(pattern = "VS", replacement = "vs", x = names(dea.limma))
colnames(dea.limma$`PND8 vs PND15`)[2:ncol(dea.limma$`PND8 vs PND15`)] <- paste("PND8_vs_PND15",
  colnames(dea.limma$`PND8 vs PND15`)[2:ncol(dea.limma$`PND8 vs PND15`)],
  sep = "_"
)
colnames(dea.limma$`PND14 vs PNW8`)[2:ncol(dea.limma$`PND14 vs PNW8`)] <- paste("PND14_vs_PNW8",
  colnames(dea.limma$`PND14 vs PNW8`)[2:ncol(dea.limma$`PND14 vs PNW8`)],
  sep = "_"
)

# Adding differential analysis results to expression table
mat_RNA <- inner_join(mat_RNA_counts_merged, dea.limma$`PND8 vs PND15`)
mat_RNA <- inner_join(mat_RNA, dea.limma$`PND14 vs PNW8`)

# Changing column names
colnames(mat_RNA)[1] <- "nearestTSS.gene_name"
colnames(mat_RNA)[2:ncol(mat_RNA)] <- paste("RNA", colnames(mat_RNA)[2:ncol(mat_RNA)], sep = "_")

# Adding RNA-Seq data to the main table
tab <- left_join(x = tab, y = mat_RNA)
```

# Genes with distanceTSS `<=` 5kb
```{r overlap-matrix-7 }
# Adding new column "Genes" to the main table with Gene symbols with distance TSS <- 2.55kb
tab$Genes <- tab$nearestTSS.gene_name
tab$Genes[abs(tab$distance2nearestTSS) > 2500] <- NA
tab <- tab[, c(1:6, ncol(tab), 7:(ncol(tab) - 1))]
```

# Save data
```{r overlap-matrix-8 }
writexl::write_xlsx(x = tab, path = "output/overlap_matrix.xlsx")
write.table(x = tab, file = gzfile("output/overlap_matrix.txt.gz"), quote = F, sep = "\t", row.names = F)
saveRDS(object = tab, file = "output/overlap_matrix.rds")
```

# References
```{r overlap-matrix-9}
report::cite_packages(session = sessionInfo())
```

# SessionInfo
```{r overlap-matrix-10 }
devtools::session_info() %>%
  details::details()
```
