---
title: "RNA-Seq: TFA analysis of SC Controls"
author: "Deepak Tanwar"
date: "<b>Created on:</b> 2019-05-13 <br> <b>Updated on:</b> `r Sys.Date()`"
output:
  rmdformats::readthedown:
    highlight: tango
    self_contained: true
    thumbnails: true
    lightbox: true
    gallery: false
    keep_md: no
    number_sections: no
    fig_width: 8
    fig_height: 8
    fig_caption: true
    df_print: paged
    code_folding: show
  fontsize: 12pt
  geometry: margin=1in
  documentclass: article
# bibliography: references.bib
link-citations: yes
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Libraries required

```{r, message=FALSE, warning=FALSE}
library(plgINS)
library(plotly)
library(UpSetR)
library(RColorBrewer)
library(grid)
library(gridExtra)
library(DT)
```

# Data
```{r}
load("input/dea_SC_Controls.DEA.RData")
load("input/voom_EList_SC_Controls.RData")
load("input/SC_controls_rnaseq_salmon.tds.RData")
data(regulon.curated.mm)
```

# TFA analysis

Transcription Factor Binding Affinity analysis is performed using Virtual Inference of Protein-activity by Enriched Regulon analysis (VIPER) algorithm.

**Regulon** is a group of genes that are regulated as a unit, generally controlled by the same regulatory gene that expresses a protein acting as a repressor or activator.

More details: [10.1038/ng.3593](https://doi.org/10.1038/ng.3593)


## Function to show table
```{r}
make_DT <- function(tab) {
  df <- data.frame(TF = rownames(tab), tab, check.names = F, stringsAsFactors = F)
  DT::datatable(
    df,
    rownames = F,
    filter = "top", extensions = c("Buttons", "ColReorder"), options = list(
      autoWidth = TRUE,
      columnDefs = list(list(
        # targets = 12:13,
        render = JS(
          "function(data, type, row, meta) {",
          "return type === 'display' && data.length > 8 ?",
          "'<span title=\"' + data + '\">' + data.substr(0, 8) + '...</span>' : data;",
          "}"
        )
      )),
      pageLength = 10,
      buttons = c("copy", "csv", "excel", "pdf", "print"),
      colReorder = list(realtime = FALSE),
      dom = "fltBip"
    )
  )
}
```

## Filters a viper regulon by likelihood
```{r}
regulon.mm10 <- regulon.filter(regulon = regulon.curated.mm, min.likelihood = 0.15)
```


## TFA analysis

### Function for running analysis
```{r}
runTFA <- function(voomEL, dea, pData, pDataCol, group1, group2, regulon) {
  s1 <- rownames(pData[pData[, pDataCol] == group1, ])
  s2 <- rownames(pData[pData[, pDataCol] == group2, ])
  se <- voomEList$E[, c(s2, s1)]
  pData <- pData[colnames(se), ]
  design <- model.matrix(~ 0 + Group, pData)
  tfa <- TFA(se = se, dea = dea, design = design, regulon = regulon)
  colData(tfa) <- DataFrame(pData)
  return(tfa)
}
```

### All regulons

```{r, warning=FALSE, message=FALSE}
tfa.all.pnd8_pnd15 <- runTFA(
  voomEL = voomEList, dea = dea.list$`PND8 vs PND15`,
  pData = salmon@phenoData, pDataCol = "Group",
  group1 = "PND8", group2 = "PND15",
  regulon = regulon.curated.mm
)

tfa.all.pnd15_adult <- runTFA(
  voomEL = voomEList, dea = dea.list$`PND15 vs Adult`,
  pData = salmon@phenoData, pDataCol = "Group",
  group1 = "PND15", group2 = "Adult",
  regulon = regulon.curated.mm
)
```


### Filtered regulons
```{r, warning=FALSE, message=FALSE}
tfa.filt.pnd8_pnd15 <- runTFA(
  voomEL = voomEList, dea = dea.list$`PND8 vs PND15`,
  pData = salmon@phenoData, pDataCol = "Group",
  group1 = "PND8", group2 = "PND15",
  regulon = regulon.mm10
)

tfa.filt.pnd15_adult <- runTFA(
  voomEL = voomEList, dea = dea.list$`PND15 vs Adult`,
  pData = salmon@phenoData, pDataCol = "Group",
  group1 = "PND15", group2 = "Adult",
  regulon = regulon.mm10
)
```


# Results

## Volcano plots {.tabset .tabset-pills}

### All regulons
```{r, fig.height=10, fig.width=8, fig.align='center', warning=F, message=FALSE}
p <- subplot(plotTFA(tfa.all.pnd8_pnd15) %>%
  layout(
    xaxis = list(range = c(-10, 10)), yaxis = list(range = c(-1, 32)),
    font = list(size = 17)
  ),
plotTFA(tfa.all.pnd15_adult) %>%
  layout(
    xaxis = list(range = c(-10, 10)), yaxis = list(range = c(-1, 32)),
    font = list(size = 17)
  ),
nrows = 2, shareX = T, shareY = T, titleX = T, titleY = T
)

p %>% layout(title = "", annotations = list(
  list(
    x = 0.5, y = 1.00, text = "PND8 vs PND15", showarrow = F, xref = "paper", yref = "paper",
    font = list(color = "red", family = "Arial", size = 20)
  ),
  list(
    x = 0.5, y = 0.5, text = "PND15 vs Adult", showarrow = F, xref = "paper", yref = "paper",
    font = list(color = "red", family = "Arial", size = 20)
  )
), showlegend = FALSE)
```

### Filtered regulons
```{r, fig.height=10, fig.width=8, fig.align='center', warning=F, message=FALSE}
p <- subplot(plotTFA(tfa.filt.pnd8_pnd15) %>%
  layout(
    xaxis = list(range = c(-10, 10)), yaxis = list(range = c(-1, 32)),
    font = list(size = 17)
  ),
plotTFA(tfa.filt.pnd15_adult) %>%
  layout(
    xaxis = list(range = c(-10, 10)), yaxis = list(range = c(-1, 32)),
    font = list(size = 17)
  ),
nrows = 2, shareX = T, shareY = T, titleX = T, titleY = T
)

p %>% layout(title = "", annotations = list(
  list(
    x = 0.5, y = 1.04, text = "PND8 vs PND15", showarrow = F, xref = "paper", yref = "paper",
    font = list(color = "red", family = "Arial", size = 20)
  ),
  list(
    x = 0.5, y = 0.5, text = "PND15 vs Adult", showarrow = F, xref = "paper", yref = "paper",
    font = list(color = "red", family = "Arial", size = 20)
  )
), showlegend = FALSE)
```


## UpSetPlot {.tabset .tabset-pills}

### All regulons
```{r, fig.align='center', fig.width=9, fig.height=8, fig.show='hide'}
tfa.all.union <- Reduce(
  union,
  list(
    rownames(rowData(tfa.all.pnd8_pnd15)[rowData(tfa.all.pnd8_pnd15)[, "activity.FDR"] <= 0.05, ]),
    rownames(rowData(tfa.all.pnd15_adult)[rowData(tfa.all.pnd15_adult)[, "activity.FDR"] <= 0.05, ])
  )
)

tfa.all.upset <- data.frame(
  TF = tfa.all.union, `PND8 vs PND15` = 0, `PND15 vs Adult` = 0,
  check.names = F, stringsAsFactors = F
)
rownames(tfa.all.upset) <- tfa.all.upset$TF


tfa.all.upset[rownames(rowData(tfa.all.pnd8_pnd15)[rowData(tfa.all.pnd8_pnd15)[, "activity.FDR"] <= 0.05, ]), 2] <- 1

tfa.all.upset[rownames(rowData(tfa.all.pnd15_adult)[rowData(tfa.all.pnd15_adult)[, "activity.FDR"] <= 0.05, ]), 3] <- 1

col <- brewer.pal(7, "Set1")


upset(tfa.all.upset[, 2:3],
  point.size = 5, sets.bar.color = col[1:2], matrix.color = col[5],
  order.by = "freq", set_size.numbers_size = T, text.scale = c(2.5, 3, 2.5, 2.5, 2.5, 3)
)

grid.edit("arrange", name = "UpSet")
vp <- grid.grab()

grid.arrange(
  grobs = list(
    vp
  ),
  top = "TFA in SC at different stages of development (FDR <= 0.05)",
  cols = 1
)
```

```{r, fig.align='center', fig.width=9, fig.height=8}
grid.arrange(
  grobs = list(
    vp
  ),
  top = "TFA in SC at different stages of development (FDR <= 0.05)",
  cols = 1
)
```



### Filtered regulons
```{r, fig.align='center', fig.width=9, fig.height=8, fig.show='hide'}
tfa.filt.union <- Reduce(
  union,
  list(
    rownames(rowData(tfa.filt.pnd8_pnd15)[rowData(tfa.filt.pnd8_pnd15)[, "activity.FDR"] <= 0.05, ]),
    rownames(rowData(tfa.filt.pnd15_adult)[rowData(tfa.filt.pnd15_adult)[, "activity.FDR"] <= 0.05, ])
  )
)

tfa.filt.upset <- data.frame(
  TF = tfa.filt.union, `PND8 vs PND15` = 0, `PND15 vs Adult` = 0,
  check.names = F, stringsAsFactors = F
)
rownames(tfa.filt.upset) <- tfa.filt.upset$TF


tfa.filt.upset[rownames(rowData(tfa.filt.pnd8_pnd15)[rowData(tfa.filt.pnd8_pnd15)[, "activity.FDR"] <= 0.05, ]), 2] <- 1

tfa.filt.upset[rownames(rowData(tfa.filt.pnd15_adult)[rowData(tfa.filt.pnd15_adult)[, "activity.FDR"] <= 0.05, ]), 3] <- 1

col <- brewer.pal(7, "Set1")


upset(tfa.filt.upset[, 2:3],
  point.size = 5, sets.bar.color = col[1:2], matrix.color = col[5],
  order.by = "freq", set_size.numbers_size = T, text.scale = c(2.5, 3, 2.5, 2.5, 2.5, 3)
)

grid.edit("arrange", name = "UpSet")
vp <- grid.grab()

grid.arrange(
  grobs = list(
    vp
  ),
  top = "TFA in SC at different stages of development (FDR <= 0.05)",
  cols = 1
)
```

```{r, fig.align='center', fig.width=9, fig.height=8}
grid.arrange(
  grobs = list(
    vp
  ),
  top = "TFA in SC at different stages of development (FDR <= 0.05)",
  cols = 1
)
```

## Heatmaps of TBA

### Data prep

#### All regulons
```{r}
tfa.all.pnd8_pnd15 <- tfa.all.pnd8_pnd15[, c(
  grep(pattern = "PND8", x = colnames(tfa.all.pnd8_pnd15), value = T),
  grep(pattern = "PND15", x = colnames(tfa.all.pnd8_pnd15), value = T)
)]

# rowData(tfa.all$`PND8 vs PND15`) <- rowData(tfa.all$`PND8 vs PND15`)[order(rowData(tfa.all$`PND8 vs PND15`)[, "activity.FDR"]), ]

tfa.all.pnd15_adult <- tfa.all.pnd15_adult[, c(
  grep(pattern = "PND15", x = colnames(tfa.all.pnd15_adult), value = T),
  grep(pattern = "Adult", x = colnames(tfa.all.pnd15_adult), value = T)
)]

# rowData(tfa$`PND15 vs Adult`) <- rowData(tfa$`PND15 vs Adult`)[order(rowData(tfa$`PND15 vs Adult`)[, 11]), ]
```


#### Filtered regulons
```{r}
tfa.filt.pnd8_pnd15 <- tfa.filt.pnd8_pnd15[, c(
  grep(pattern = "PND8", x = colnames(tfa.filt.pnd8_pnd15), value = ),
  grep(pattern = "PND15", x = colnames(tfa.filt.pnd8_pnd15), value = )
)]

# rowData(tfa$`PND8 vs PND15`) <- rowData(tfa$`PND8 vs PND15`)[order(rowData(tfa$`PND8 vs PND15`)[, 11]), ]


tfa.filt.pnd15_adult <- tfa.filt.pnd15_adult[, c(
  grep(pattern = "PND15", x = colnames(tfa.filt.pnd15_adult), value = ),
  grep(pattern = "Adult", x = colnames(tfa.filt.pnd15_adult), value = )
)]

# rowData(tfa$`PND8 vs Adult`) <- rowData(tfa$`PND8 vs Adult`)[order(rowData(tfa$`PND8 vs Adult`)[, 11]), ]
```

## Heatmaps

### All regulons {.tabset .tabset-pills}

#### PND8 vs PND15
```{r}
sehm(
  hmcols = viridis::viridis(1000), se = tfa.filt.pnd8_pnd15[1:20, ],
  anno_columns = c("Group", "Cage.ID", "Batch"), scale = "row",
  anno_rows = c("activity.logFC", "activity.FDR", "targetEnrichment", "targetEnrFDR")
)
```

#### PND15 vs Adult
```{r}
sehm(
  hmcols = viridis::viridis(1000), se = tfa.all.pnd15_adult[1:20, ],
  anno_columns = c("Group", "Cage.ID", "Batch"), scale = "row",
  anno_rows = c("activity.logFC", "activity.FDR", "targetEnrichment", "targetEnrFDR")
)
```


### Filtered regulons {.tabset .tabset-pills}

#### PND8 vs PND15
```{r}
sehm(
  hmcols = viridis::viridis(1000), se = tfa.filt.pnd8_pnd15[1:20, ],
  anno_columns = c("Group", "Cage.ID", "Batch"), scale = "row",
  anno_rows = c("activity.logFC", "activity.FDR", "targetEnrichment", "targetEnrFDR")
)
```


#### PND15 vs Adult
```{r}
sehm(
  hmcols = viridis::viridis(1000), se = tfa.filt.pnd15_adult[1:20, ],
  anno_columns = c("Group", "Cage.ID", "Batch"), scale = "row",
  anno_rows = c("activity.logFC", "activity.FDR", "targetEnrichment", "targetEnrFDR")
)
```


## Tables

### All regulons {.tabset .tabset-pills}

#### PND8 vs PND15
```{r}
make_DT(tab = rowData(tfa.all.pnd8_pnd15))
write.table(data.frame(rowData(tfa.all.pnd8_pnd15)),
  "./output/all_regulons_pnd8_pnd15.txt",sep = "\t",
  quote = F, row.names = T
)
```

#### PND15 vs Adult
```{r}
make_DT(tab = rowData(tfa.all.pnd15_adult))
write.table(data.frame(rowData(tfa.all.pnd15_adult)),
  "./output/all_regulons_pnd15_adults.txt",sep = "\t",
  quote = F, row.names = T
)
```


### Filtered regulons {.tabset .tabset-pills}

#### PND8 vs PND15
```{r}
make_DT(tab = rowData(tfa.filt.pnd8_pnd15))
write.table(data.frame(rowData(tfa.filt.pnd8_pnd15)),
  "./output/filt_regulons_pnd8_pnd15.txt",sep = "\t",
  quote = F, row.names = T
)
```

#### PND15 vs Adult
```{r}
make_DT(tab = rowData(tfa.filt.pnd15_adult))
write.table(data.frame(rowData(tfa.filt.pnd15_adult)),
  "./output/filt_regulons_pnd15_adults.txt", sep = "\t",
  quote = F, row.names = T
)
```


# SessionInfo
```{r}
devtools::session_info()
```
