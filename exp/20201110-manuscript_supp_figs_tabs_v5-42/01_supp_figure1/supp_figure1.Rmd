---
title: "Supp. Figure1"
author: "Deepak Tanwar"
date: "<b>Created on:</b> 2020-10-22 <br> <b>Updated on:</b> `r Sys.Date()`"
output:
  rmdformats::readthedown:
    highlight: tango
    self_contained: true
    thumbnails: true
    lightbox: true
    gallery: false
    keep_md: no
    number_sections: no
    fig_width: 8
    fig_height: 8
    fig_caption: true
    df_print: paged
    code_folding: hide
  fontsize: 12pt
  geometry: margin=1in
  documentclass: article
# bibliography: references.bib
link-citations: yes
---


# Libraries and functions
```{r}
source("../bin/functions.R")
```


# C
```{r}
sheets <- readxl::excel_sheets("input/Fig.S1_C_Cell.markers.for.heatmap.xlsx")
names(sheets) <- sheets

files <- lapply(sheets, function(x) data.frame(read_xlsx("input/Fig.S1_C_Cell.markers.for.heatmap.xlsx", sheet = x, col_names = FALSE)))

df <- ldply(files)
colnames(df) <- c("Cell markers", "Genes")
df$Genes[df$Genes == "Ngn3"] <- "Neurog3"
rownames(df) <- df$Genes

rm <- "Sox2|Dppa3|Nanog"

df <- df[grep(pattern = rm, x = df$Genes, invert = T),]

load("input/data_pData.RData")

ge <- df$Genes

expr <- data.frame(Genes = rownames(data$cpm), data$cpm)

mat <- merge(df, expr)

mat <- data$cpm[ge, ]
mat <- mat[, c(grep(pattern = "PND8|PND15", x = colnames(mat)), grep(pattern = "PND14|PNW8", x = colnames(mat)))]
# mat <- data.matrix(mat - rowMeans(mat[, grep(pattern = "PND8", x = colnames(mat), value = T)]))

o <- seriate(max(mat) - mat)
# o <- seriate(dist(mat), method = "MDS_angle")

mat <- mat[get_order(o), ]

cd <- data$pData[colnames(mat), 1, drop = FALSE]
cd$Group <- factor(cd$Group, levels = unique(cd$Group))

df <- df[rownames(mat), ]
df$`Cell markers` <- factor(df$`Cell markers`, names(sheets))

rd_col <- brewer.pal(n = length(unique(df$`Cell markers`)) + 1, name = "Dark2")[c(1:4,6)]
names(rd_col) <- unique(df$`Cell markers`)

cr <- colorRamp2(breaks = c(-5,0,5), colors = c("blue", "white", "red"))

ha <- HeatmapAnnotation(
    df = cd[, 1, drop = FALSE],
    col = list(Group = cols),
    simple_anno_size = unit(3, "mm"),
    show_annotation_name = TRUE,
    annotation_name_side = "right",
    annotation_name_rot = 0,
    annotation_name_gp = gpar(fontsize = 0, fontfamily = "Helvetica"),
    annotation_legend_param = list(
      title_gp = gpar(fontsize = 10, fontface = "plain", fontfamily = "Helvetica"),
      labels_gp = gpar(fontsize = 9, fontfamily = "Helvetica")
    )
  )

set.seed(1100)
c <- ha %v% Heatmap(
  matrix = mat, 
  # km = 2,
  col = hm_cols(mat, col = c("blue", "white", "red")),
  cluster_rows = F,
  cluster_columns = T,
  clustering_method_columns = "ward.D2",
  show_row_names = T,
  show_column_names = F,
  name = "Enrichment (log)",
  split = df$`Cell markers`,
  column_split = cd[, 1, drop = FALSE],
  column_gap = unit(c(1,2.5,1), "mm"),
  cluster_column_slices = F,
  show_parent_dend_line = FALSE,
  row_title = NULL,
  row_names_gp = gpar(fontsize = 9, fontfamily = "Helvetica"),
  column_title = NULL,
  column_dend_height = unit(0.5, "cm"),
  use_raster = FALSE,
  left_annotation = HeatmapAnnotation(
    df = df[, 1, drop = FALSE],
    which = "row",
    col = list('Cell markers' = rd_col),
    simple_anno_size = unit(3, "mm"),
    show_annotation_name = TRUE,
    # annotation_name_side = "left",
    annotation_name_rot = 0,
    annotation_name_gp = gpar(fontsize = 0, fontfamily = "Helvetica"),
    annotation_legend_param = list(
      title_gp = gpar(fontsize = 10, fontface = "plain", fontfamily = "Helvetica"),
      labels_gp = gpar(fontsize = 9, fontfamily = "Helvetica")
    )
  ),
  top_annotation = ha,
  height = unit(x = 6, units = "cm"), width = unit(x = 4, units = "cm"),
  heatmap_legend_param = list(
    title = expression(Log[2] ~ "CPM"),
    legend_height = unit(1, "cm"),
    # title_position = "topleft",
    title_gp = gpar(fontsize = 10, fontfamily = "Helvetica"),
    labels_gp = gpar(fontsize = 9, fontfamily = "Helvetica")
  )
)


pdf(file = "output/c.pdf", width = 8.5, height = 11)
draw(
  object = c,
  heatmap_legend_side = "right",
  annotation_legend_side = "right",
  merge_legends = TRUE,
  align_annotation_legend = ""
)
dev.off()


png(filename = "output/c.png", width = 8.5, height = 11, units = "in", res = 330)
draw(
  object = c,
  heatmap_legend_side = "right",
  annotation_legend_side = "right",
  merge_legends = TRUE,
  align_annotation_legend = ""
)
dev.off()
```



# References
```{r overlap-matrix-rGREAT-43 }
report::cite_packages(session = sessionInfo())
```

# SessionInfo
```{r overlap-matrix-rGREAT-44 }
devtools::session_info() %>%
  details::details()
```
